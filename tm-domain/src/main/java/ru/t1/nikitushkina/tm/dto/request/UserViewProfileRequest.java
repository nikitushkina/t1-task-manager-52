package ru.t1.nikitushkina.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class UserViewProfileRequest extends AbstractUserRequest {

    public UserViewProfileRequest(@Nullable String token) {
        super(token);
    }

}
