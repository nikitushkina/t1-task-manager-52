package ru.t1.nikitushkina.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class SystemInfoRequest extends AbstractUserRequest {

    public SystemInfoRequest(@Nullable String token) {
        super(token);
    }

}
