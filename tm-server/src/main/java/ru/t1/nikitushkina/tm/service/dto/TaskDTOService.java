package ru.t1.nikitushkina.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.nikitushkina.tm.api.service.IConnectionService;
import ru.t1.nikitushkina.tm.api.service.dto.ITaskDTOService;
import ru.t1.nikitushkina.tm.dto.model.TaskDTO;
import ru.t1.nikitushkina.tm.enumerated.Status;
import ru.t1.nikitushkina.tm.exception.entity.TaskNotFoundException;
import ru.t1.nikitushkina.tm.exception.field.DescriptionEmptyException;
import ru.t1.nikitushkina.tm.exception.field.IdEmptyException;
import ru.t1.nikitushkina.tm.exception.field.NameEmptyException;
import ru.t1.nikitushkina.tm.exception.field.StatusEmptyException;
import ru.t1.nikitushkina.tm.exception.user.UserIdEmptyException;
import ru.t1.nikitushkina.tm.repository.dto.TaskDTORepository;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public final class TaskDTOService extends AbstractUserOwnedDTOService<TaskDTO, ITaskDTORepository>
        implements ITaskDTOService {

    public TaskDTOService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public void changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            return repository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    protected ITaskDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskDTORepository(entityManager);
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateProjectIdById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(userId, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
