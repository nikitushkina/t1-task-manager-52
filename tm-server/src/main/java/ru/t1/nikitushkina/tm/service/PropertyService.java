package ru.t1.nikitushkina.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String APPLICATION_FILE_NAME_KEY = "config";
    @NotNull
    public static final String APPLICATION_FILE_NAME_DEFAULT = "application.properties";
    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";
    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";
    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";
    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "42";
    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";
    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "1488";
    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";
    @NotNull
    public static final String EMPTY_VALUE = "-";
    @NotNull
    public static final String GIT_BRANCH = "gitBranch";
    @NotNull
    public static final String GIT_COMMIT_ID = "gitCommitId";
    @NotNull
    public static final String GIT_COMMIT_TIME = "gitCommitTime";
    @NotNull
    public static final String GIT_COMMIT_MSG_FULL = "gitCommitMsgFull";
    @NotNull
    public static final String GIT_COMMITTER_NAME = "gitCommitterName";
    @NotNull
    public static final String GIT_COMMITTER_EMAIL = "gitCommitterEmail";
    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";
    @NotNull
    private static final String SERVER_PORT_DEFAULT = "8080";
    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";
    @NotNull
    private static final String SERVER_HOST_DEFAULT = "0.0.0.0";
    @NotNull
    private static final String SESSION_KEY_KEY = "session.key";
    @NotNull
    private static final String SESSION_TIMEOUT_KEY = "session.timeout";
    @NotNull
    private static final String SESSION_TIMEOUT_DEFAULT = "10800";
    @NotNull
    private static final String DATABASE_USERNAME = "database.username";
    @NotNull
    private static final String DATABASE_PASSWORD = "database.password";
    @NotNull
    private static final String DATABASE_URL = "database.url";
    @NotNull
    private static final String DATABASE_DRIVER = "database.driver";
    @NotNull
    private static final String DATABASE_USERNAME_DEFAULT = "postgres";
    @NotNull
    private static final String DATABASE_PASSWORD_DEFAULT = "postgres";
    @NotNull
    private static final String DATABASE_URL_DEFAULT = "jdbc:postgresql://localhost:5432/tm";
    @NotNull
    private static final String DATABASE_DRIVER_DEFAULT = "org.postgresql.Driver";
    @NotNull
    private static final String DATABASE_DIALECT = "database.dialect";
    @NotNull
    private static final String DATABASE_SHOW_SQL = "database.show_sql";
    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO = "database.hbm2ddl_auto";
    @NotNull
    private static final String DATABASE_DIALECT_DEFAULT = "org.hibernate.dialect.PostgreSQLDialect";
    @NotNull
    private static final String DATABASE_SHOW_SQL_DEFAULT = "true";
    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO_DEFAULT = "update";
    @NotNull
    private static final String FORMAT_SQL = "database.format_sql";
    @NotNull
    private static final String FORMAT_SQL_DEFAULT = "true";
    @NotNull
    private static final String DATABASE_SECOND_LVL_CACHE = "database.second_lvl_cache";
    @NotNull
    private static final String DATABASE_SECOND_LVL_CACHE_DEFAULT = "true";
    @NotNull
    private static final String DATABASE_FACTORY_CLASS = "database.factory_class";
    @NotNull
    private static final String DATABASE_FACTORY_CLASS_DEFAULT = "com.hazelcast.hibernate.HazelcastCacheRegionFactory";
    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE = "database.use_query_cache";
    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE_DEFAULT = "true";
    @NotNull
    private static final String DATABASE_USE_MIN_PUTS = "database.use_min_puts";
    @NotNull
    private static final String DATABASE_USE_MIN_PUTS_DEFAULT = "true";
    @NotNull
    private static final String DATABASE_REGION_PREFIX = "database.region_prefix";
    @NotNull
    private static final String DATABASE_REGION_PREFIX_DEFAULT = "t1-task-manager";
    @NotNull
    private static final String DATABASE_CONFIG_FILE_PATH = "database.config_file_path";
    @NotNull
    private static final String DATABASE_CONFIG_FILE_PATH_DEFAULT = "hazelcast.xml";
    @NotNull
    private static final String DATABASE_SCHEMA_NAME = "database.default_schema";
    @NotNull
    private static final String DATABASE_SCHEMA_NAME_DEFAULT = "PUBLIC";
    @NotNull
    private static final String TOKEN_INIT = "database.init_token";
    @NotNull
    private static final String LIQUIBASE_CONFIG = "database.liquibase_config";
    @NotNull
    private static final String LIQUIBASE_CONFIG_DEFAULT = "database.liquibase_config";
    @NotNull
    private static final String JMS_URL = "jms.url";
    @NotNull
    private static final String JMS_URL_DEFAULT = "tcp://127.0.0.1:61616";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final boolean existsConfig = isExistExternalConfig();
        if (existsConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @NotNull
    @Override
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getCommitId() {
        return read(GIT_COMMIT_ID);
    }

    @NotNull
    @Override
    public String getCommitMsgFull() {
        return read(GIT_COMMIT_MSG_FULL);
    }

    @NotNull
    @Override
    public String getCommitTime() {
        return read(GIT_COMMIT_TIME);
    }

    @NotNull
    @Override
    public String getCommitterEmail() {
        return read(GIT_COMMITTER_EMAIL);
    }

    @NotNull
    @Override
    public String getCommitterName() {
        return read(GIT_COMMITTER_NAME);
    }

    @NotNull
    @Override
    public String getDBConfigFilePath() {
        return getStringValue(DATABASE_CONFIG_FILE_PATH, DATABASE_CONFIG_FILE_PATH_DEFAULT);
    }

    @NotNull
    @Override
    public String getLiquibaseConfig() {
        return getStringValue(LIQUIBASE_CONFIG, LIQUIBASE_CONFIG_DEFAULT);
    }

    @NotNull
    @Override
    public String getJmsUrl() {
        return getStringValue(JMS_URL, JMS_URL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBDialect() {
        return getStringValue(DATABASE_DIALECT, DATABASE_DIALECT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBDriver() {
        return getStringValue(DATABASE_DRIVER, DATABASE_DRIVER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBFactoryClass() {
        return getStringValue(DATABASE_FACTORY_CLASS, DATABASE_FACTORY_CLASS_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBHbm2ddlAuto() {
        return getStringValue(DATABASE_HBM2DDL_AUTO, DATABASE_HBM2DDL_AUTO_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBPassword() {
        return getStringValue(DATABASE_PASSWORD, DATABASE_PASSWORD_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBRegionPrefix() {
        return getStringValue(DATABASE_REGION_PREFIX, DATABASE_REGION_PREFIX_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBSchema() {
        return getStringValue(DATABASE_SCHEMA_NAME, DATABASE_SCHEMA_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBSecondLvlCache() {
        return getStringValue(DATABASE_SECOND_LVL_CACHE, DATABASE_SECOND_LVL_CACHE_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBShowSql() {
        return getStringValue(DATABASE_SHOW_SQL, DATABASE_SHOW_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBUrl() {
        return getStringValue(DATABASE_URL, DATABASE_URL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBUseMinPuts() {
        return getStringValue(DATABASE_USE_MIN_PUTS, DATABASE_USE_MIN_PUTS_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBUseQueryCache() {
        return getStringValue(DATABASE_USE_QUERY_CACHE, DATABASE_USE_QUERY_CACHE_DEFAULT);
    }

    @NotNull
    @Override
    public String getDBUser() {
        return getStringValue(DATABASE_USERNAME, DATABASE_USERNAME_DEFAULT);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    @Override
    public String getFormatSQL() {
        return getStringValue(FORMAT_SQL, FORMAT_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getGitBranch() {
        return read(GIT_BRANCH);
    }

    @NotNull
    private Integer getIntegerValue(
            @NotNull final String key,
            @NotNull final String defaultKey
    ) {
        return Integer.parseInt(getStringValue(key, defaultKey));
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerPort() {
        return getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY_KEY);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT_KEY, SESSION_TIMEOUT_DEFAULT);
    }

    @NotNull
    private String getStringValue(
            @NotNull final String key,
            @NotNull final String defaultValue
    ) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getTokenInit() {
        return getStringValue(TOKEN_INIT, EMPTY_VALUE);
    }

    private boolean isExistExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @SneakyThrows
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @NotNull final InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

}
